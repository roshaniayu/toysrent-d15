from django.shortcuts import render
from django.db import connection
import json
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import random
import time
import datetime 

# Create your views here.
def create_pesanan(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to toys_rent, public')
    cursor.execute("select P.nama_lengkap from pengguna P, anggota A where P.no_ktp = A.no_ktp")
    hasil1 = cursor.fetchall()
    response['anggota'] = hasil1
    cursor.execute("select nama_item from Barang")
    hasil2 = cursor.fetchall()
    response['barang'] = hasil2
    return render(request,'create_pesanan.html',response)

def post_create_pesanan(request):
    if(request.method=="POST"):
        cursor = connection.cursor()
        cursor.execute('set search_path to toys_rent, public')
        lama=request.POST['lama']
        list_barang=request.POST.getlist('Barang')

        
        if (request.session['role'] == "admin"):
            anggota = request.POST['anggota']
            cursor.execute("Select no_ktp from pengguna where nama_lengkap = '" + anggota + "'")
            no_ktp = cursor.fetchone()[0]

        if (request.session['role'] == "anggota"):
            no_ktp = request.session['ktp']

        id_pemesanan = random.randint(1000000000,9999999999)
        ongkir = random.randint(10000,10000)          
        ts = time.gmtime()
        now = time.strftime("%Y-%m-%d %H:%M:%S", ts)

        cursor.execute("select level from anggota where no_ktp = '" + str(no_ktp)+ "'")
        level = cursor.fetchone()[0]
        status = "Being confirmed"

        sewa = 0
        for i in list_barang:
            cursor.execute("select harga_sewa from info_barang_level I, barang B where B.id_barang = I.id_barang AND B.nama_item = '" + i + "' AND " + "I.nama_level = '" + level + "'")
            sewa = sewa + cursor.fetchone()[0]

        cursor.execute("INSERT INTO pemesanan(id_pemesanan,datetime_pesanan,kuantitas_barang,harga_sewa,ongkos,no_ktp_pemesan,status) values("+"'"+str(id_pemesanan)+"','" +now+"','"+str(len(list_barang))+"',"+str(sewa)+","+str(ongkir)+ ",'" + no_ktp + "','" + status +"')")
        cursor.execute("select no_urut from barang_pesanan order by no_urut desc limit 1")
        next_urut = int(cursor.fetchone()[0])

        for i in list_barang:
            # cursor.execute("select id_stasiun from stasiun where nama ='"+i+"' ")
            # id_stasiun = cursor.fetchone()
            cursor.execute("select id_barang from barang where nama_item = '" + i + "'")
            id_barang = cursor.fetchone()[0]           
            next_urut = next_urut + 1
            today = datetime.date.today() 
            panjangwaktu = datetime.timedelta(days=int(lama))  
            pengembalian = today + panjangwaktu  

            cursor.execute("INSERT INTO barang_pesanan(id_pemesanan,no_urut,id_barang,tanggal_sewa,lama_sewa,tanggal_kembali,status) values("+"'"+str(id_pemesanan)+"','"+str(next_urut)+ "','" + str(id_barang) + "','" + str(today) + "'," + str(int(lama)) + ",'" + str(pengembalian) + "','Being confirmed" +"');")

        # messages.error(request, "success")
        return HttpResponseRedirect('/pesanan/daftar_pesanan')
    else:
        # messages.error(request, "gagal")
        return HttpResponseRedirect('/pesanan/create_pesanan')

def daftar_pesanan(request):
    cursor = connection.cursor()
    cursor.execute('''select nama_item from toys_rent.barang''')
    list_barang = cursor.fetchall()
    cursor.execute('''select nama from toys_rent.status''')
    list_status = cursor.fetchall()
    response = {'listbarang' : list_barang, 'liststatus' : list_status}
    return render(request,'daftar_pesanan.html',response)

def get_daftar_pesanan(request):
    try:
        q = request.GET['q']
    except:
        q = ""
    print(q)
    if (request.session['role'] == "anggota"):
        no_ktp = request.session['ktp']
        if q == "":
            sql = "SELECT P.id_pemesanan, B.nama_item, PE.harga_sewa, P.status, P.lama_sewa FROM toys_rent.barang_pesanan P, toys_rent.barang B, toys_rent.pemesanan PE where p.id_barang = b.id_barang AND P.id_pemesanan = PE.id_pemesanan AND PE.no_ktp_pemesan = '" + str(no_ktp) + "'"
        else:
            sql = "SELECT P.id_pemesanan, B.nama_item, PE.harga_sewa, P.status, P.lama_sewa FROM toys_rent.barang_pesanan P, toys_rent.barang B, toys_rent.pemesanan PE where p.id_barang = b.id_barang AND P.id_pemesanan = PE.id_pemesanan AND lower(P.id_pemesanan) like %s AND PE.no_ktp_pemesan = '" + str(no_ktp) + "'"  
    else:
        if q == "":
            sql = "SELECT P.id_pemesanan, B.nama_item, PE.harga_sewa, P.status, P.lama_sewa FROM toys_rent.barang_pesanan P, toys_rent.barang B, toys_rent.pemesanan PE where p.id_barang = b.id_barang AND P.id_pemesanan = PE.id_pemesanan"
        else:
            sql = "SELECT P.id_pemesanan, B.nama_item, PE.harga_sewa, P.status, P.lama_sewa FROM toys_rent.barang_pesanan P, toys_rent.barang B, toys_rent.pemesanan PE where p.id_barang = b.id_barang AND P.id_pemesanan = PE.id_pemesanan AND lower(P.id_pemesanan) like %s"
    cursor = connection.cursor()
    q = '%%' + q + '%%' 
    q = q.lower()
    cursor.execute(sql, (q,))
    list_pesanan = cursor.fetchall()
    dict_pesanan = {}
    for i in list_pesanan:
        if i[0] in dict_pesanan:
            dict_pesanan[i[0]]["daftar_barang"].append(i[1])
        else:
            dict_pesanan[i[0]] = {}
            dict_pesanan[i[0]]["id_pemesanan"] = i[0]
            dict_pesanan[i[0]]["daftar_barang"] = [str(i[1])]
            dict_pesanan[i[0]]["harga_sewa"] = i[2]
            dict_pesanan[i[0]]["status"] = i[3]
    return JsonResponse(dict_pesanan)

@csrf_exempt
def delete_pesanan(request):
    namanya = request.POST['id']
    cursor = connection.cursor()
    sql = "delete from toys_rent.pemesanan where id_pemesanan = %s"
    cursor.execute(sql, (namanya,))
    connection.commit()
    return JsonResponse({})