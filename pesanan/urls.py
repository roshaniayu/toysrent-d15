"""toysrent URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('create_pesanan', views.create_pesanan, name='create_pesanan'),
    path('daftar_pesanan', views.daftar_pesanan, name='daftar_pesanan'),
    path('delete_pesanan', views.delete_pesanan, name='delete_pesanan'),
    path('get-pesanan-json/', views.get_daftar_pesanan, name='get-daftar-pesanan'),
    path('post_create_pesanan/',views.post_create_pesanan, name = 'postcreatepesanan'),
]
