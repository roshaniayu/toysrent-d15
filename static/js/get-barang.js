// String.prototype.format = function() {
//     var a = this;
//     for (var k in arguments) {
//         a = a.replace(new RegExp("\\{" + k + "\\}", 'g'), arguments[k]);
//     }
//     return a
// }

// function updateURLParameter(url, param, paramVal) {
//     var newAdditionalURL = "";
//     var tempArray = url.split("?");
//     var baseURL = tempArray[0];
//     var additionalURL = tempArray[1];
//     var temp = "";
//     if (additionalURL) {
//         tempArray = additionalURL.split("&");
//         for (var i = 0; i < tempArray.length; i++) {
//             if (tempArray[i].split('=')[0] != param) {
//                 newAdditionalURL += temp + tempArray[i];
//                 temp = "&";
//             }
//         }
//     }

//     var rows_txt = temp + "" + param + "=" + paramVal;
//     return baseURL + "?" + newAdditionalURL + rows_txt;
// }

// var td_open = "<td scope='row'>";
// var td_close = "</td>";
// var tr_open = "<tr>";
// var tr_open_format = "<tr id='id{0}'>";
// var tr_close = "</tr>";
// var update_button = '<button type="button" class="btn btn-primary update" data-toggle="modal" data-target="#{0}"><i class="fas fa-sync-alt"></i></button>';
// var modal_update = `
// <div class="modal fade" id="{0}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
//     <div class="modal-dialog modal-dialog-centered" role="document">
//         <div class="modal-content">
//             <div class="modal-header">
//                 <h5 class="modal-title">Update Barang</h5>
//                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
//                     <span aria-hidden="true">&times;</span>
//                 </button>
//             </div>

//             <!-- TODO bikin django-forms nya, update function di views nya, set default nya bisa get by id-->
//             <div class="modal-body modalkiri">
//                 <form action="#">
//                     <div class="form-label-group">
//                         <label for="inputIdBarang"><b>ID Barang:</b></label>
//                         <input type="text" id="inputIdBarang" class="form-control" value="1112" required>
//                     </div>
//                     <div class="form-label-group">
//                         <label for="inputNamaItem"><b>Nama Item:</b></label>
//                         <select class="custom-select" id="inputNamaItem" name="listNamaItem" required>
//                             <option selected>Infinity Gauntlet v.2</option>
//                             <option value="1">item_1</option>
//                             <option value="2">item_2</option>
//                             <option value="3">item_3</option>
//                             <option value="4">item_4</option>
//                         </select>
//                     </div>
//                     <div class="form-label-group jarakatas">
//                         <label for="inputRentangUsia"><b>Warna:</b></label>
//                         <input type="text" id="inputRentangUsia" class="form-control" value="Ungu Thanos" required>
//                     </div>
//                     <div class="form-label-group jarakatas">
//                         <label for="inputBahan"><b>URL Foto:</b></label>
//                         <input type="text" id="inputBahan" class="form-control" value="bit.ly/contohGambar" required>
//                     </div>
//                     <div class="form-label-group jarakatas">
//                         <label for="inputRentangUsia"><b>Kondisi:</b></label>
//                         <input type="text" id="inputRentangUsia" class="form-control" value="Sedang disewa anggota" required>
//                     </div>
//                     <div class="form-label-group jarakatas">
//                         <label for="inputBahan"><b>Lama Penggunaan:</b></label>
//                         <input type="text" id="inputBahan" class="form-control" value="5 tahun" required>
//                     </div>
//                     <div class="form-label-group jarakatas">
//                         <label for="inputBahan"><b>Pemilik/Penyewa:</b></label>
//                         <input type="text" id="inputBahan" class="form-control" value="Thanos" required>
//                     </div>
//                     <hr>
//                     <b>Info Barang</b><br>
//                     <ul>
//                         <li>Bronze<br> Persen royalty:
//                             <div class="infobarang">
//                                 <input type="text" id="inputBahan" class="form-control" value="5" required>
//                                 <div class="infobarang2">
//                                     (%)
//                                 </div>
//                             </div>
//                             Harga sewa:
//                             <div class="infobarang3">
//                                 <input type="text" id="inputBahan" class="form-control" value="30.000" required>
//                                 <div class="infobarang2">
//                                     (Rupiah/minggu)
//                                 </div>
//                             </div>
//                         </li>
//                         <li>Silver<br> Persen royalty:
//                             <div class="infobarang">
//                                 <input type="text" id="inputBahan" class="form-control" value="10" required>
//                                 <div class="infobarang2">
//                                     (%)
//                                 </div>
//                             </div>
//                             Harga sewa:
//                             <div class="infobarang3">
//                                 <input type="text" id="inputBahan" class="form-control" value="25.000" required>
//                                 <div class="infobarang2">
//                                     (Rupiah/minggu)
//                                 </div>
//                             </div>
//                         </li>
//                         <li>Gold<br> Persen royalty:
//                             <div class="infobarang">
//                                 <input type="text" id="inputBahan" class="form-control" value="15" required>
//                                 <div class="infobarang2">
//                                     (%)
//                                 </div>
//                             </div>
//                             Harga sewa:
//                             <div class="infobarang3">
//                                 <input type="text" id="inputBahan" class="form-control" value="20.000" required>
//                                 <div class="infobarang2">
//                                     (Rupiah/minggu)
//                                 </div>
//                             </div>
//                         </li>
//                     </ul>
//                 </div>

//                 <div class="modal-footer">
//                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
//                 <button type="submit" class="btn btn-primary btnagree">Update</button>
//                 </form>
//             </div>
//         </div>
//     </div>
// </div>
// `;
// var delete_button = '<button type="button" class="btn btn-primary delete" data-toggle="modal" data-target="#{0}"><i class="fas fa-trash"></i></button>';
// var confirm_delete = `                             
// <div class="modal fade" id="{0}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
//     <div class="modal-dialog modal-dialog-centered" role="document">
//         <div class="modal-content">
//             <div class="modal-header">
//                 <h5 class="modal-title" id="deleteBarang">Delete Barang</h5>
//                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
//                     <span aria-hidden="true">&times;</span>
//                 </button>
//             </div>
//             <div class="modal-body modalkiri">
//                 Apakah benar ingin <b>menghapus</b> Barang {1} {2}?
//             </div>
//             <div class="modal-footer">
//                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
//                 <button type="button" class="btn btn-primary btnagree" data-dismiss="modal" onclick="delete_entry('{3}')">Ya,
//                     benar</button>
//             </div>
//         </div>
//     </div>
// </div>
// `;
// var detail_barang = `
// <button type="button" class="btn btn-primary detail"><a href="/barang/detail_barang/{0}">Lihat Detail Barang</a></button>
// `
// var pagination_open = `
//     <nav aria-label="Page navigation example">
//         <ul class="pagination">
//         `
// var pagination_close = `
//         </ul>
//         </nav>
//         `
// var pagination_item = `
//         <li class="page-item"><a class="page-link" href="{0}">{1}</a></li>
// `
// var url;
// var page;
// var sorter;

// $(document).ready(function() {
//     url = new URL(window.location.href);
//     page = url.searchParams.get("page");
//     sorter = url.searchParams.get("sort");
//     console.log(sorter);
//     if (sorter != null) {
//         $("#sorter_select").val(sorter);
//         if (sorter == "id_barang") sorter += "::bigint";
//     }
//     $.ajax({
//         url: url_list_barang,
//         method: "POST",
//         data: {
//             page_number: page,
//             sorter: sorter
//         },
//         success: function(result) {
//             // tampilkan tabel
//             var str_build = "";
//             for (var result_per_line in result['result']) {
//                 var id_barang = result['result'][result_per_line][0];

//                 str_build += tr_open_format.format(id_barang);

//                 // untuk string
//                 for (var cell in result['result'][result_per_line]) {
//                     str_build += td_open;
//                     if (result['result'][result_per_line][cell] != null) {
//                         str_build += result['result'][result_per_line][cell];
//                     }
//                     str_build += td_close;
//                 }

//                 // untuk tombol
//                 str_build += td_open;
//                 str_build += update_button.format('update' + id_barang);
//                 str_build += modal_update.format('update' + id_barang);

//                 var daftar_item = result['result'][result_per_line][1];
//                 var warna = result['result'][result_per_line][2];
//                 str_build += delete_button.format('delete' + id_barang);
//                 str_build += confirm_delete.format('delete' + id_barang, daftar_item, 'warna ' + warna, id_barang);
//                 str_build += td_close;


//                 // untuk detail
//                 str_build += td_open;
//                 str_build += detail_barang.format(id_barang);
//                 str_build += td_close;

//                 str_build += tr_close;
//             }
//             $("#tabel-barang").append(str_build);

//             // tampilkan pagination
//             var page_built = "";
//             page_built += pagination_open;
//             if (page == null) {
//                 page = "1";
//             }
//             var page_int = parseInt(page, 10)
//             if (page_int > parseInt("1", 10)) {
//                 page_built += pagination_item.format(updateURLParameter(window.location.href, 'page', page_int - 1), 'Previous');
//             }
//             for (var counter = 1; counter <= result['pagination']; counter++) {
//                 page_built += pagination_item.format(updateURLParameter(window.location.href, 'page', counter), counter);
//             }
//             if (page_int < result['pagination']) {
//                 page_built += pagination_item.format(updateURLParameter(window.location.href, 'page', page_int + 1), 'Next');
//             }
//             page_built += pagination_close;
//             $("#pagination").append(page_built);

//         },
//         error: function(error_msg) {
//             console.log(error_msg);
//         }
//     });
// });

// function delete_entry(id_number) {
//     $(".delete").attr("disabled", true);
//     $.ajax({
//         type: "POST",
//         url: url_del_barang,
//         data: {
//             id: id_number,
//         },
//         success: function(result) {
//             $("#id" + id_number).remove();
//             if (page == null) {
//                 page = "1";
//             }
//             var page_int = parseInt(page, 10)
//             if (!$.trim($('#tabel-barang').html()).length) {
//                 if (page_int == 1) {
//                     // redirect to tell empty table
//                 } else {
//                     windows.location.replace('?page=' + (page_int - 1));
//                 }
//             }
//             $(".delete").attr("disabled", false);
//         },
//         error: function(result) {
//             alert('error');
//         }
//     });
// }

// function sort(sort_type) {
//     var sort_type_to_pass = sort_type;
//     if (sort_type == "id_barang") {
//         sort_type_to_pass += "::bigint";
//     }
//     $.ajax({
//         url: url_list_barang,
//         type: "POST",
//         data: {
//             page_number: page,
//             sorter: sort_type_to_pass
//         },
//         success: function(result) {
//             // ganti url
//             window.history.replaceState('', '', updateURLParameter(window.location.href, "sort", sort_type));
//             // tampilkan tabel
//             $("#tabel-barang").empty();
//             var str_build = "";
//             for (var result_per_line in result['result']) {
//                 var id_barang = result['result'][result_per_line][0];

//                 str_build += tr_open_format.format(id_barang);

//                 // untuk string
//                 for (var cell in result['result'][result_per_line]) {
//                     str_build += td_open;
//                     if (result['result'][result_per_line][cell] != null) {
//                         str_build += result['result'][result_per_line][cell];
//                     }
//                     str_build += td_close;
//                 }

//                 // untuk tombol
//                 str_build += td_open;
//                 str_build += update_button.format('update' + id_barang);
//                 str_build += modal_update.format('update' + id_barang);

//                 var daftar_item = result['result'][result_per_line][1];
//                 var warna = result['result'][result_per_line][2];
//                 str_build += delete_button.format('delete' + id_barang);
//                 str_build += confirm_delete.format('delete' + id_barang, daftar_item, warna, id_barang);
//                 str_build += td_close;


//                 // untuk detail
//                 str_build += td_open;
//                 str_build += detail_barang.format(id_barang);
//                 str_build += td_close;

//                 str_build += tr_close;
//             }
//             $("#tabel-barang").append(str_build);
//         },
//         error: function(result) {
//             alert('error');
//         }
//     });
// }