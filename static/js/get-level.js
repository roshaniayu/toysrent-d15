$(document).ready(function() {
    var counter = 0
    $.get({
      url: get_level_json_url + "?q=",
      context: document.body,
      success: function(data) {
          console.log(data)
          console.log("aa")
        $.each(data, function(i, item) {
            console.log(item)
          $("#tabel-level").append(
            `
            <tr>
                <td scope="row">${ ++counter }</td>
                <td scope="row">${ item["nama_level"]}</td>
                <td scope="row">${ item["deskripsi"]}</td>
                <td scope="row">${ item["minimum_poin"]}</td>
                <td scope="row">
                    <button type="button" class="btn btn-primary update" data-toggle="modal" data-target="#update${ item["nama_level"]}">
                        <i class="fas fa-sync-alt"></i>
                            </button>
                                    <div class="modal fade" id="update${ item["nama_level"]}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="updateLevel${ item["nama_level"]}">Update Level</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body modalkiri">
                                                        <form action="#">
                                                            <div class="form-label-group">
                                                                <label for="inputNamaLevel"><b>Nama Level:</b></label>
                                                                <input type="text" id="inputNamaLevel${ item["nama_level"]}" class="form-control" value="${ item["nama_level"]}" required>
                                                            </div>
                                                            <div class="form-label-group jarakatas">
                                                                <label for="inputDeskripsiLevel"><b>Deskripsi:</b></label>
                                                                <input type="text" id="inputDeskripsiLevel${ item["nama_level"]}" class="form-control" value="${ item["deskripsi"]}" required>
                                                            </div>
                                                            <div class="form-label-group jarakatas">
                                                                <label for="inputMinimumPoin"><b>Minimum Poin:</b></label>
                                                                <input type="text" id="inputMinimumPoin" class="form-control" value="${ item["minimum_poin"]}" required>
                                                            </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                        <button type="submit" class="btn btn-primary btnagree">Update</button>
                                                        </form>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-primary delete" data-toggle="modal" data-target="#delete${ item["nama_level"]}">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                            <div class="modal fade" id="delete${ item["nama_level"]}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="deleteLevel1">Delete Level</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body modalkiri">
                                                        Apakah benar ingin <b>menghapus</b> Level ${ item["nama_level"]}?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                                                        <button type="button" onclick="deleteFunction('${ item["nama_level"]}')" class="btn btn-primary btnagree">Ya, benar</button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

            `)
        });
      },
      error: function(xhr) {
        console.log("aaa");
        console.log(xhr);
    }
    });
  });


  $('#searchbar').on("keyup", function(e){
    q = e.currentTarget.value
    var counter = 0
    console.log(q)
    $.get({
      url: get_level_json_url + "?q=" + q,
      context: document.body,
      success: function(data) {
          console.log(data)
          console.log("aa")
          $("#tabel-level").html('')
        $.each(data, function(i, item) {
            console.log(item)
          $("#tabel-level").append(
            `
            <tr>
                <td scope="row">${ ++counter }</td>
                <td scope="row">${ item["nama_level"]}</td>
                <td scope="row">${ item["deskripsi"]}</td>
                <td scope="row">${ item["minimum_poin"]}</td>
                <td scope="row">
                    <button type="button" class="btn btn-primary update" data-toggle="modal" data-target="#update1">
                        <i class="fas fa-sync-alt"></i>
                            </button>
                                    <div class="modal fade" id="update1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="updateLevel1">Update Level</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body modalkiri">
                                                        <form action="#">
                                                            <div class="form-label-group">
                                                                <label for="inputNamaLevel"><b>Nama Level:</b></label>
                                                                <input type="text" id="inputNamaLevel${ item["nama_level"]}" class="form-control" value="${ item["nama_level"]}" required>
                                                            </div>
                                                            <div class="form-label-group jarakatas">
                                                                <label for="inputDeskripsiLevel"><b>Deskripsi:</b></label>
                                                                <input type="text" id="inputDeskripsiLevel${ item["nama_level"]}" class="form-control" value="${ item["deskripsi"]}" required>
                                                            </div>
                                                            <div class="form-label-group jarakatas">
                                                                <label for="inputMinimumPoin"><b>Minimum Poin:</b></label>
                                                                <input type="text" id="inputMinimumPoin${ item["nama_level"]}" class="form-control" value="${ item["minimum_poin"]}" required>
                                                            </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                        <button type="submit" class="btn btn-primary btnagree">Update</button>
                                                        </form>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-primary delete" data-toggle="modal" data-target="#delete1">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                            <div class="modal fade" id="delete1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="deleteLevel1">Delete Level</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body modalkiri">
                                                        Apakah benar ingin <b>menghapus</b> Level ${ item["nama_level"]}?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                                                        <button type="button" class="btn btn-primary btnagree">Ya, benar</button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

            `)
        });
      },
      error: function(xhr) {
        console.log("aaa");
        console.log(xhr);
    }
    });
  });