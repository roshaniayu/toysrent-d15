$(document).ready(function() {
    var counter = 0
    $.get({
      url: get_pesanan_json_url + "?q=",
      context: document.body,
      success: function(data) {
          console.log(data)
          console.log("aa")
        $.each(data, function(i, item) {
            console.log(item)
          $("#tabel-pesanan").append(
            `
            <tr>
                <td scope="row">${ item["id_pemesanan"]}</td>
                <td scope="row">${ item["daftar_barang"]}</td>
                <td scope="row">Rp.${ item["harga_sewa"]}</td>
                <td scope="row">${ item["status"]}</td>
                <td scope="row">
                    <button type="button" class="btn btn-primary update" data-toggle="modal" data-target="#update${ item["id_pemesanan"]}">
                        <i class="fas fa-sync-alt"></i>
                            </button>
                                    <div class="modal fade" id="update${ item["id_pemesanan"]}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="updatePemesanan${ item["id_pemesanan"]}">Update Pemesanan</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body modalkiri">
                                                        <form action="#">
                                                        <div class="form-label-group jarakatas">
                                                        <label for="inputAnggota"><b>Barang:</b></label>
                                                        <select name="Barang" class="form-control selectpicker" id="inputBarang" placeholder="Pilih Barang" multiple data-live-search="true" required>
                                                            {% for i in barang %}
                                                                <option>{{i.0}}</option>
                                                            {% endfor %}
                                                        </select>
                                                        </div>
                                                    <div class="form-label-group jarakatas">
                                                        <label for="inputLama"><b>Lama Sewa:</b></label>
                                                        <input name="lama" type="text" id="inputLama" class="form-control" required>
                                                    </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                        <button type="submit" class="btn btn-primary btnagree">Update</button>
                                                        </form>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-primary delete" data-toggle="modal" data-target="#delete${ item["id_pemesanan"]}">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                            <div class="modal fade" id="delete${ item["id_pemesanan"]}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="deletePemesanan1">Delete Pemesanan</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body modalkiri">
                                                        Apakah benar ingin <b>menghapus</b> Pemesanan ${ item["id_pemesanan"]}?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                                                        <button type="button" onclick="deleteFunction('${ item["id_pemesanan"]}')" class="btn btn-primary btnagree">Ya, benar</button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

            `)
        });
      },
      error: function(xhr) {
        console.log("aaa");
        console.log(xhr);
    }
    });
  });


  $('#searchbar').on("keyup", function(e){
    q = e.currentTarget.value
    var counter = 0
    console.log(q)
    $.get({
      url: get_pesanan_json_url + "?q=" + q,
      context: document.body,
      success: function(data) {
          console.log(data)
          console.log("aa")
          $("#tabel-pesanan").html('')
        $.each(data, function(i, item) {
            console.log(item)
          $("#tabel-pesanan").append(
            `
            <tr>
                <td scope="row">${ item["id_pemesanan"]}</td>
                <td scope="row">${ item["daftar_barang"]}</td>
                <td scope="row">Rp.${ item["harga_sewa"]}</td>
                <td scope="row">${ item["status"]}</td>
                <td scope="row">
                    <button type="button" class="btn btn-primary update" data-toggle="modal" data-target="#update1">
                        <i class="fas fa-sync-alt"></i>
                            </button>
                                    <div class="modal fade" id="update${ item["id_pemesanan"]}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="updatePemesanan${ item["id_pemesanan"]}">Update Pemesanan</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body modalkiri">
                                                        <form action="#">
                                                        <div class="form-label-group jarakatas">
                                                        <label for="inputAnggota"><b>Barang:</b></label>
                                                        <select name="Barang" class="form-control selectpicker" id="inputBarang" placeholder="Pilih Barang" multiple data-live-search="true" required>
                                                            {% for i in barang %}
                                                                <option>{{i.0}}</option>
                                                            {% endfor %}
                                                        </select>
                                                        </div>
                                                    <div class="form-label-group jarakatas">
                                                        <label for="inputLama"><b>Lama Sewa:</b></label>
                                                        <input name="lama" type="text" id="inputLama" class="form-control" required>
                                                    </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                        <button type="submit" class="btn btn-primary btnagree">Update</button>
                                                        </form>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-primary delete" data-toggle="modal" data-target="#delete${ item["id_pemesanan"]}">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                            <div class="modal fade" id="delete${ item["id_pemesanan"]}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="deletePemesanan1">Delete Pemesanan</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body modalkiri">
                                                        Apakah benar ingin <b>menghapus</b> Pemesanan ${ item["id_pemesanan"]}?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                                                        <button type="button" onclick="deleteFunction('${ item["id_pemesanan"]}')" class="btn btn-primary btnagree">Ya, benar</button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

            `)
        });
      },
      error: function(xhr) {
        console.log("aaa");
        console.log(xhr);
    }
    });
  });