from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

# Create your views here.
def home(request):
    response = {}
    return render(request,'home.html',response)

def login(request):
    response = {}
    if ('nama' in request.session.keys()):
        return HttpResponseRedirect('/')
    return render(request,'login.html',response)

def post_login(request):
    ktp = str(request.POST['ktp'])
    email = request.POST['email']

    cursor = connection.cursor()
    cursor.execute('set search_path to toys_rent,public')
    cursor.execute("SELECT * from pengguna where no_ktp='"+ktp+"' and email='"+email+"'")
    hasil=cursor.fetchone()
    if(hasil):
        cursor.execute("SELECT * from admin where no_ktp='"+ktp+"'")
        hasil2 = cursor.fetchone()
        if(hasil2):
            cursor.execute("SELECT nama_lengkap from pengguna where no_ktp='"+ktp+"'")
            nama_lengkap = cursor.fetchall()
            for row in nama_lengkap:
                nama_lengkap = row[0]
            cursor.execute("SELECT tanggal_lahir from pengguna where no_ktp='"+ktp+"'")
            tanggal_lahir = cursor.fetchall()
            tanggal_lahir = str(tanggal_lahir[0])
            print(tanggal_lahir)
            cursor.execute("SELECT no_telp from pengguna where no_ktp='"+ktp+"'")
            no_telp = cursor.fetchall()
            for row in no_telp:
                no_telp = row[0]
            request.session['ktp'] = ktp
            request.session['nama'] = nama_lengkap
            request.session['email'] = email
            request.session['tanggal'] = tanggal_lahir
            request.session['telp'] = no_telp
            request.session['role'] = 'admin'
            return HttpResponseRedirect('/profil/')
        else:
            cursor.execute("SELECT * from anggota where no_ktp='"+ktp+"'")
            hasil3 = cursor.fetchone()
            if(hasil3):
                cursor.execute("SELECT nama_lengkap from pengguna where no_ktp='"+ktp+"'")
                nama_lengkap = cursor.fetchall()
                for row in nama_lengkap:
                    nama_lengkap = row[0]
                cursor.execute("SELECT tanggal_lahir from pengguna where no_ktp='"+ktp+"'")
                tanggal_lahir = cursor.fetchall()
                tanggal_lahir = str(tanggal_lahir[0])
                print(tanggal_lahir)
                cursor.execute("SELECT no_telp from pengguna where no_ktp='"+ktp+"'")
                no_telp = cursor.fetchall()
                for row in no_telp:
                    no_telp = row[0]
                request.session['nama'] = nama_lengkap
                request.session['ktp'] = ktp
                request.session['nama'] = nama_lengkap
                request.session['email'] = email
                request.session['tanggal'] = tanggal_lahir
                request.session['telp'] = no_telp
                cursor.execute("SELECT poin from anggota where no_ktp='"+ktp+"'")
                poin = cursor.fetchall()
                for row in poin:
                    poin = str(row[0])
                cursor.execute("SELECT level from anggota where no_ktp='"+ktp+"'")
                level = cursor.fetchall()
                for row in level:
                    level = row[0]
                request.session['poin'] = poin
                request.session['level'] = level
                cursor.execute("SELECT nama from alamat where no_ktp_anggota='"+ktp+"'")
                nama_alamat = cursor.fetchall()
                for row in nama_alamat:
                    nama_alamat = row[0]
                cursor.execute("SELECT jalan from alamat where no_ktp_anggota='"+ktp+"'")
                jalan = cursor.fetchall()
                for row in jalan:
                    jalan = row[0]
                cursor.execute("SELECT nomor from alamat where no_ktp_anggota='"+ktp+"'")
                nomor = cursor.fetchall()
                for row in nomor:
                    nomor = str(row[0])
                cursor.execute("SELECT kota from alamat where no_ktp_anggota='"+ktp+"'")
                kota = cursor.fetchall()
                for row in kota:
                    kota = row[0]
                cursor.execute("SELECT kodepos from alamat where no_ktp_anggota='"+ktp+"'")
                kodepos = cursor.fetchall()
                for row in kodepos:
                    kodepos = str(row[0])
                request.session['nama_alamat'] = nama_alamat
                request.session['jalan'] = jalan
                request.session['nomor'] = nomor
                request.session['kota'] = kota
                request.session['kodepos'] = kodepos
                request.session['role'] = 'anggota'

                return HttpResponseRedirect('/profil/')
            else:
                return HttpResponseRedirect('/login/')
    else:
        return HttpResponseRedirect('/login/')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect('/')