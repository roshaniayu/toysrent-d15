"""toysrent URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('login.urls')),
    path('register/', include('register.urls')),
    path('item/', include(('item.urls','item'),namespace='item')),
    path('profil/', include('profil.urls')),
    path('pesanan/', include(('pesanan.urls','pesanan'),namespace='pesanan')),
    path('level/', include(('level.urls','level'),namespace='level')),
    path('barang/', include(('barang.urls','barang'),namespace='barang')),
    path('chat/', include('chat.urls')),
]
