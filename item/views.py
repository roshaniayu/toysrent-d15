from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def create_item(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to toys_rent, public')
    cursor.execute("select nama from kategori")
    hasil1 = cursor.fetchall()
    response['kategori'] = hasil1
    return render(request,'create_item.html',response)

def daftar_item(request):
    response={}
    cursor = connection.cursor()
    cursor.execute('set search_path to toys_rent, public')
    cursor.execute("SELECT K.nama_item, K.nama_kategori, I.deskripsi, I.usia_dari, I.usia_sampai, I.bahan FROM kategori_item K, item I WHERE I.nama = K.nama_item")
    hasil1 = cursor.fetchall()
    response['item'] = hasil1
    cursor.execute("select nama from kategori")
    hasil2 = cursor.fetchall()
    response['kategori'] = hasil2
    return render(request,'daftar_item.html',response)

def post_create_item(request):
    if(request.method=="POST"):
        nama = request.POST['nama']
        deskripsi = request.POST['deskripsi']
        minusia= request.POST['rentangusia1']
        maxusia= request.POST['rentangusia2']
        bahan=request.POST['bahan']
        list_kategori=request.POST.getlist('kategori')

        cursor = connection.cursor()
        cursor.execute('set search_path to toys_rent')
        cursor.execute("SELECT * from item where nama='"+nama+"'")
        hasil = cursor.fetchone()
        if(hasil):
            if len(hasil) > 0:
                return HttpResponseRedirect('/item/create_item/')
        cursor.execute("INSERT INTO item(nama,deskripsi,usia_dari,usia_sampai,bahan) values("+"'"+nama+"','" +deskripsi+"','"+str(minusia)+"',"+str(maxusia)+",'"+bahan+"');")
        for i in list_kategori:
            cursor.execute("INSERT INTO kategori_item(nama_item,nama_kategori) values("+"'"+nama+"','"+i+"');")

        return HttpResponseRedirect('/item/daftar_item/')
    else:
        return HttpResponseRedirect('/item/create_item/')

@csrf_exempt
def delete_item(request):
    if(request.method=="POST"):
        id_item = request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to toys_rent, public')
        cursor.execute("DELETE FROM kategori_item WHERE nama_item='"+id_item+"';")
        cursor.execute("DELETE FROM item WHERE nama='"+id_item+"';")
        return HttpResponseRedirect('/item/daftar_item/')
    else:
        return HttpResponseRedirect('/item/daftar_item/')

@csrf_exempt
def update_item(request):
    if(request.method=="POST"):
        nama = request.POST['nama']
        deskripsi = request.POST['deskripsi']
        minusia= request.POST['rentangusia1']
        maxusia= request.POST['rentangusia2']
        bahan=request.POST['bahan']
        list_kategori=request.POST.getlist('kategori')

        cursor = connection.cursor()
        cursor.execute('set search_path to toys_rent, public')
        cursor.execute("UPDATE item SET deskripsi='"+deskripsi+"', usia_dari='"+minusia+"', usia_sampai='"+maxusia+"', bahan='"+bahan+"' where nama = '"+nama+"';")

        cursor.execute("DELETE FROM kategori_item WHERE nama_item='"+nama+"';")
        for i in list_kategori:
            cursor.execute("INSERT INTO kategori_item(nama_item,nama_kategori) values("+"'"+nama+"','"+i+"');")

        return HttpResponseRedirect('/item/daftar_item/')
    else:
        return HttpResponseRedirect('/item/daftar_item/')