from django.urls import path
from . import views

urlpatterns = [
    path('', views.chat, name='chat'),
    path('<tujuan>', views.chat, name='chat_tujuan'),
]
