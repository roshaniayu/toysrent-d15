from django import forms
from django.db import connection


def get_admin_list():
    cursor = connection.cursor()
    sql = "select no_ktp, nama_lengkap from toys_rent.admin natural join toys_rent.pengguna"
    cursor.execute(sql)
    list_admin = cursor.fetchall()
    return list_admin

class Chat_Form(forms.Form):
    # untuk tujuan
    attrs_tujuan = {
        'class': 'form-control',
        'id': 'form_ID'
    }
    tujuan_input = forms.CharField(
        label='',
        required=True,
        widget=forms.TextInput(attrs=attrs_tujuan)
    )

    # pengirim ambil dari session

    # untuk isi teks / chat
    attrs_isi = {
        'class': 'form-control',
        'id': 'form_ID'
    }
    isi_input = forms.CharField(
        label='',
        required=True,
        widget=forms.Textarea(attrs=attrs_tujuan)
    )

    # pilih admin
    attrs_select = {
        'class': 'custom-select',
        'placeholder': 'Masukan warna barang',
        'id': 'form_item'
    }
    # perlu antisipasi item kosong
    list_admin = get_admin_list() 
    admin_input = forms.ChoiceField(
        label='Nama Item',
        choices=list_admin,
        required=True,
        widget=forms.Select(attrs=attrs_select)
    )