from django.db import connection
from django.shortcuts import render
from .forms import Chat_Form
from datetime import datetime

response = {}


def chat(request, tujuan = "0"):
    no_ktp_anggota = "1"
    try:
        no_ktp_anggota = request.session['ktp']
    except:
        # belum terimplementasi
        None

    # ambil daftar nama admin:
    cursor = connection.cursor()
    sql = "select nama_lengkap, no_ktp from toys_rent.admin natural join toys_rent.pengguna"
    cursor.execute(sql)
    daftar_admin = cursor.fetchall()
    response['daftar_admin'] = daftar_admin
    response['nama_admin'] = daftar_admin[0][0]

    ktp_admin = ""
    if (tujuan == 0):
        ktp_admin = daftar_admin[0][1]
    else:
        ktp_admin = tujuan
    # ambil total pesannya
    cursor = connection.cursor()
    sql = "select count(*) from toys_rent.chat where no_ktp_anggota = %s and no_ktp_admin = %s group by id"
    cursor.execute(sql, (
        no_ktp_anggota,
        ktp_admin
    ))
    total_pesan = cursor.fetchall() or "0"
    response['total_pesan'] = total_pesan

    # ambil history pesan orang ini
    cursor = connection.cursor()
    sql = "select pesan, datetime from toys_rent.chat where no_ktp_anggota = %s and no_ktp_admin = %s group by id"
    cursor.execute(sql, (
        no_ktp_anggota, 
        ktp_admin
    ))
    messages = cursor.fetchall()
    response['messages'] = messages

    # response['chatform'] = Chat_Form
    # if request.method == 'POST':
    #     form = Chat_Form(request.POST or None)
    #     if (form.is_valid()):
    #         # ambil values
    #         tujuan = request.POST.get('tujuan_input')
    #         isi = request.POST.get('isi_input')
    #         admin = request.POST.get('admin_input')
    #         timestamp = datetime.today().strftime('%Y-%m-%d %H:%M:%S')

    #         cursor = connection.cursor()
    #         sql = "SELECT MAX(id::bigint) FROM toys_rent.chat"
    #         cursor.execute(sql)
    #         largest_id = cursor.fetchall()[0][0]

    #         id_submit = largest_id + 1
    #         pesan = "tujuan : %s \n %s", tujuan, isi
    #         datetime_submit = timestamp

    #         no_ktp_admin = admin

    #         # masukin ke db
    #         cursor = connection.cursor()
    #         sql = "INSERT INTO toys_rent.chat VALUES(%s, %s, %s, %s, %s)"
    #         cursor.execute(sql, (
    #             id_submit,
    #             pesan,
    #             datetime_submit,
    #             no_ktp_anggota,
    #             no_ktp_anggota,
    #             no_ktp_admin
    #         )
    #         )
    return render(request, 'chat.html', response)
