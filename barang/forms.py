from django import forms
from django.db import connection


def get_item_list():
    cursor = connection.cursor()
    sql = "select nama from toys_rent.item"
    cursor.execute(sql)
    list_nama_item = cursor.fetchall()
    return list_nama_item

# TODO
def get_pemilik_list():
    cursor = connection.cursor()
    sql = "SELECT no_ktp, nama_lengkap FROM toys_rent.anggota NATURAL JOIN toys_rent.pengguna"
    cursor.execute(sql)
    list_nama_pengguna = cursor.fetchall()
    return list_nama_pengguna


# beberapa ada yang diset default, perlu diatur
# beberapa ada yang ambil data dari db, perlu diatur
class Barang_Form(forms.Form):
    # untuk input id
    attrs_id = {
        'class': 'form-control',
        'placeholder': 'Masukan ID barang',
        'id': 'form_ID'
    }
    id_input = forms.CharField(
        label='ID Barang',
        required=True,
        max_length=10, 
        widget=forms.TextInput(attrs=attrs_id)
    )

    # untuk input nama item
    attrs_select = {
        'class': 'custom-select',
        'placeholder': 'Masukan warna barang',
        'id': 'form_item'
    }
    # perlu antisipasi item kosong
    list_nama_item = get_item_list() 
    list_nama_item_real = []
    for x in list_nama_item:
        for y in x:
            list_nama_item_real.append(((y),(y)))
    item_input = forms.ChoiceField(
        label='Nama Item',
        choices=list_nama_item_real,
        required=True,
        widget=forms.Select(attrs=attrs_select)
    )

    # untuk input warna
    attrs_warna = {
        'class': 'form-control',
        'placeholder': 'Masukan warna barang',
        'id': 'form_warna'
    }
    warna_input = forms.CharField(
        label='Warna',
        max_length=50,
        required=False,
        widget=forms.TextInput(attrs=attrs_warna)
    )

    # untuk input url
    attrs_url = {
        'class': 'form-control',
        'placeholder': 'Masukan url foto',
        'id': 'form_url'
    }
    url_input = forms.URLField(
        label='URL foto',
        required=False,
        widget=forms.TextInput(attrs=attrs_url)
    )

    # untuk input kondisi
    # kondisi bisa dibuat default ?
    attrs_kondisi = {
        'class': 'form-control',
        'placeholder': 'Masukan kondisi barang',
        'id': 'form_kondisi'
    }
    kondisi_input = forms.CharField(
        label='Kondisi',
        required=True,
        widget=forms.TextInput(attrs=attrs_kondisi)
    )

    # untuk input lama penggunaan
    attrs_lama_penggunaan = {
        'class': 'form-control',
        'placeholder': 'Masukan lama penggunaan',
        'id': 'form_lama_penggunaan'
    }
    lama_penggunaan_input = forms.IntegerField(
        label='Lama Penggunaan',
        required=False,
        widget=forms.TextInput(attrs=attrs_lama_penggunaan)
    )

    # untuk input nomor ktp
    # nomor ktp pemilik: max_length=20, ambil dari profil
    attrs_select_pemilik = {
        'class': 'custom-select',
        'placeholder': 'Nomor KTP Pemilik',
        'id': 'form_warna'
    }
    list_pemilik = get_pemilik_list() 
    pemilik_input = forms.ChoiceField(
        label='Identitas Pemilik',
        choices=list_pemilik,
        required=True,
        widget=forms.Select(attrs=attrs_select_pemilik)
    )

class Info_Barang_Level_Form(forms.Form):

    # untuk input persen royalti
    attrs_royal = {
        'class': 'form-control',
        'id': 'form_ID'
    }
    royal_input = forms.DecimalField(
        label='',
        required=True,
        widget=forms.NumberInput(attrs=attrs_royal)
    )

    # untuk harga sewa
    attrs_harga_sewa = {
        'class': 'form-control',
        'id': 'form_ID'
    }
    harga_sewa = forms.DecimalField(
        label='',
        required=True,
        widget=forms.NumberInput(attrs=attrs_harga_sewa)
    )

class Review_Form(forms.Form):
    # untuk input persen royalti
    attrs_review = {
        'class': 'form-control',
        'id': 'form_ID'
    }
    review_input = forms.CharField(
        label='',
        required=True,
        widget=forms.Textarea(attrs=attrs_review)
    )