from django.shortcuts import render, redirect
from django.urls import reverse
from django.db import connection
from .forms import Barang_Form, Info_Barang_Level_Form, Review_Form
from django.http import JsonResponse, HttpResponse, Http404, HttpResponseNotFound
import json
from urllib.parse import unquote
from django.views.decorators.csrf import csrf_exempt

response = {}


def create_barang(request):
    response['barangform'] = Barang_Form
    # buat fieldform sebanyak level, ambil dari db
    cursor = connection.cursor()
    sql = "SELECT COUNT(*) FROM toys_rent.level_keanggotaan"
    cursor.execute(sql)
    total_level = cursor.fetchall()[0][0]

    # ambil nama - nama level nya
    cursor = connection.cursor()
    sql = "SELECT nama_level FROM toys_rent.level_keanggotaan ORDER BY nama_level ASC"
    cursor.execute(sql)
    nama_nama_level = cursor.fetchall()
    nama_nama_level_clear = []
    for x in range(0, len(nama_nama_level)-1):
        for y in nama_nama_level[x]:
            nama_nama_level_clear.append(y)
    
    # bikin form sebanyak total_level
    formers = []
    for x in range(0, total_level-1):
        newInfo = Info_Barang_Level_Form
        formers.append([newInfo,nama_nama_level_clear[x]])
    response['formers'] = formers

    if request.method == 'POST':
        form = Barang_Form(request.POST or None)
        if (form.is_valid()):
            # ambil values dari POST request
            id_barang = request.POST.get("id_input")
            nama_item = request.POST.get("item_input")
            warna = request.POST.get("warna_input")
            warna = warna if warna != '' else None
            url_foto = request.POST.get("url_input")
            url_foto = url_foto if url_foto != '' else None
            kondisi = request.POST.get("kondisi_input")
            lama_penggunaan = request.POST.get("lama_penggunaan_input")
            lama_penggunaan = lama_penggunaan if lama_penggunaan != '' else None
            no_ktp_penyewa = request.POST.get("pemilik_input")

            # masukan ke db
            # perlu catch bila id sudah digunakan
            cursor = connection.cursor()
            sql = "INSERT INTO toys_rent.barang VALUES (%s, %s, %s, %s, %s, %s, %s);"
            cursor.execute(sql, (
                id_barang,
                nama_item,
                warna,
                url_foto,
                kondisi,
                lama_penggunaan,
                no_ktp_penyewa
            )
            )

            # redirect ke halaman paling akhir untuk nampilkan yang baru
            cursor = connection.cursor()
            sql = "SELECT COUNT(*) FROM toys_rent.barang"
            cursor.execute(sql)
            last_page = cursor.fetchall()[0][0]
            if (last_page % 10 == 0):
                last_page = last_page / 10
            else:
                last_page = last_page // 10 + 1
            last_page = int(last_page)
            site = unquote(reverse('barang:daftar_barang_pagination', kwargs={
                           'page_number': last_page}))
            return redirect(site)
    return render(request, 'create_barang.html', response)


def daftar_barang(request, page_number=1, sorter=""):
    response['barangform'] = Barang_Form
    # buat fieldform sebanyak level, ambil dari db
    cursor = connection.cursor()
    sql = "SELECT COUNT(*) FROM toys_rent.level_keanggotaan"
    cursor.execute(sql)
    total_level = cursor.fetchall()[0][0]

    # ambil nama - nama level nya
    cursor = connection.cursor()
    sql = "SELECT nama_level FROM toys_rent.level_keanggotaan ORDER BY nama_level ASC"
    cursor.execute(sql)
    nama_nama_level = cursor.fetchall()
    nama_nama_level_clear = []
    for x in range(0, len(nama_nama_level)-1):
        for y in nama_nama_level[x]:
            nama_nama_level_clear.append(y)
    
    # bikin form sebanyak total_level
    formers = []
    for x in range(0, total_level-1):
        newInfo = Info_Barang_Level_Form
        formers.append([newInfo,nama_nama_level_clear[x]])
    response['formers'] = formers
    
    if request.method == 'POST':
        form = Barang_Form(request.POST or None)
        if (form.is_valid()):
             # ambil values dari POST request
            id_barang = request.POST.get("id_input")
            nama_item = request.POST.get("item_input")
            warna = request.POST.get("warna_input")
            warna = warna if warna != '' else None
            url_foto = request.POST.get("url_input")
            url_foto = url_foto if url_foto != '' else None
            kondisi = request.POST.get("kondisi_input")
            lama_penggunaan = request.POST.get("lama_penggunaan_input")
            lama_penggunaan = lama_penggunaan if lama_penggunaan != '' else None
            no_ktp_penyewa = request.POST.get("pemilik_input")

            # masukan ke db
            # perlu catch bila id sudah digunakan
            cursor = connection.cursor()
            sql = "UPDATE toys_rent.barang SET nama_item = %s, warna = %s, url_foto = %s, kondisi = %s, lama_penggunaan = %s, no_ktp_penyewa = %s WHERE id_barang = %s;"
            cursor.execute(sql, (
                nama_item,
                warna,
                url_foto,
                kondisi,
                lama_penggunaan,
                no_ktp_penyewa,
                id_barang
            )
            )

            # redirect ke halaman awal
            site = unquote(reverse('barang:daftar_barang'))
            return redirect(site)

    return render(request, 'daftar_barang.html', response)


def detail_barang(request, id_barang = 1):
    cursor = connection.cursor()
    sql = "SELECT id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan, nama_lengkap FROM toys_rent.barang JOIN toys_rent.pengguna ON toys_rent.barang.no_ktp_penyewa = toys_rent.pengguna.no_ktp WHERE id_barang = CAST(" + str(id_barang) + " AS VARCHAR(10));"
    cursor.execute(sql)
    try:
        detail_info = cursor.fetchall()[0]
    except:
        return HttpResponseNotFound("Sorry, the page you looking for is invalid or not found\nHint: don't use address bar directly")
    response['id_barang'] = detail_info[0]
    response['nama_item'] = detail_info[1]
    response['warna'] = detail_info[2]
    response['url_foto'] = detail_info[3]
    response['kondisi'] = detail_info[4]
    response['lama_penggunaan'] = detail_info[5]
    response['nama_lengkap'] = detail_info[6]
    response['review_form'] = Review_Form
    # ambil review:
    cursor = connection.cursor()
    sql = "select nama_lengkap, review from toys_rent.pengguna p, toys_rent.barang_dikirim b, toys_rent.pengiriman k where k.no_resi = b.no_resi and p.no_ktp = k.no_ktp_anggota and id_barang = CAST(" + str(id_barang) + " AS VARCHAR(10));"
    cursor.execute(sql)
    response['daftar_review'] = cursor.fetchall()
    return render(request, 'detail_barang.html', response)


@csrf_exempt
def get_barang_list(request):
    sql = "SELECT id_barang, nama_item, warna, kondisi FROM toys_rent.barang"
    # ambil page number
    page_number = 1
    try:
        page_number = int(request.POST.get('page_number'))
    except:
        page_number = 1

    # cek sort type
    sorter = None
    try:
        sorter = request.POST.get('sorter')
        if (sorter != ""):
            sql += " ORDER BY " + sorter
    except:
        sorter = None

    # ambil list 10 barang, dimulai dari offset
    cursor = connection.cursor()
    if (page_number < 1):
        # TODO
        None
    offset = (page_number-1) * 10
    sql += " LIMIT 10 OFFSET " + str(offset)
    cursor.execute(sql)
    list_barang = cursor.fetchall()
    # ambil total pagination dari count
    cursor = connection.cursor()
    sql = "SELECT COUNT(*) FROM toys_rent.barang"
    cursor.execute(sql)
    total_pagination = cursor.fetchall()[0][0]
    if (total_pagination % 10 == 0):
        total_pagination = total_pagination / 10
    else:
        total_pagination = total_pagination // 10 + 1

    # return response
    response = {
        'result': list_barang,
        'pagination': total_pagination
    }
    return JsonResponse(response)


@csrf_exempt
def delete_barang(request):
    id_barang = request.POST.get("id")
    sql = "DELETE FROM toys_rent.barang WHERE id_barang = CAST(" + id_barang + " AS VARCHAR(10));"
    cursor = connection.cursor()
    cursor.execute(sql)
    return JsonResponse({'result': 'complete'})
