from django.urls import path
from .views import create_barang, daftar_barang, detail_barang, get_barang_list, delete_barang

urlpatterns = [
    path('create_barang', create_barang, name='create_barang'),
    path('daftar_barang/', daftar_barang, name='daftar_barang'),
    path('daftar_barang/?page=<int:page_number>', daftar_barang, name='daftar_barang_pagination'),
    path('detail_barang/<int:id_barang>', detail_barang, name='detail_barang_second'),
    path('get_barang_list', get_barang_list, name='get_barang_list'),
    path('delete_barang', delete_barang, name='delete_barang'),
]
