from django.shortcuts import render
from django.db import connection
from .forms import Level_Form
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def create_level(request):
    response = {}
    response['levelform'] = Level_Form

    if request.method == 'POST':
        form = Level_Form(request.POST or None)
        if (form.is_valid()):
            nama = request.POST.get("nama")
            deskripsi = request.POST.get("deskripsi")
            poin = request.POST.get("poin")
            cursor = connection.cursor()
            sql = "INSERT INTO toys_rent.level_keanggotaan VALUES (%s, %s, %s);"
            cursor.execute(sql,(nama,poin,deskripsi))
            return render(request, 'daftar_level.html', response)
    return render(request,'create_level.html',response)

@csrf_exempt
def daftar_level(request):
    cursor = connection.cursor()
    sql = "select nama_level, deskripsi, minimum_poin from toys_rent.level_keanggotaan"
    cursor.execute(sql)
    list_level = cursor.fetchall()
    response = {'level' : list_level}
    return render(request,'daftar_level.html',response)

@csrf_exempt
def get_level_json(request):
    try:
        q = request.GET['q']
    except:
        q = ""
    print(q)
    if q == "":
        sql = "select nama_level, deskripsi, minimum_poin from toys_rent.level_keanggotaan"
    else:
        sql = "select nama_level, deskripsi, minimum_poin from toys_rent.level_keanggotaan where lower(nama_level) like %s"
    cursor = connection.cursor()
    q = '%%' + q + '%%' 
    q = q.lower()
    cursor.execute(sql, (q,))
    list_level = cursor.fetchall()
    dict_level = {}
    for i in list_level:
        b = i[0]
        dict_level[i[0]] = {}
        dict_level[i[0]]["nama_level"] = i[0]
        dict_level[i[0]]["deskripsi"] = i[1]
        dict_level[i[0]]["minimum_poin"] = i[2]
    return JsonResponse(dict_level)

@csrf_exempt
def delete_level(request):
    namanya = request.POST['namalevel']
    cursor = connection.cursor()
    sql = "delete from toys_rent.level_keanggotaan where nama_level = %s"
    cursor.execute(sql, (namanya,))
    connection.commit()
    return JsonResponse({})
