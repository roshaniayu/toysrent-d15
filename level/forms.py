from django import forms

class Level_Form(forms.Form):
    attrsnama = {
        'class' : 'form-control',
        'placeholder' : 'Masukan nama level',
        'id' : 'formNama'
    }

    attrsdeskripsi = {
        'class' : 'form-control',
        'placeholder' : 'Masukan deskripsi level',
        'id' : 'formDeskripsi'
    }

    attrspoin = {
        'class' : 'form-control',
        'placeholder' : 'Masukan minimal poin',
        'id' : 'formPoin'
    }

    nama = forms.CharField(label='Nama Level', required=True, max_length=30, widget=forms.TextInput(attrs=attrsnama))
    deskripsi = forms.CharField(label='Deskripsi', required=True, max_length=40, widget=forms.TextInput(attrs=attrsdeskripsi))
    poin = forms.IntegerField(label='Minimal Poin', required=True, widget=forms.TextInput(attrs=attrspoin))
