from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

# Create your views here.
def register(request):
    response = {}
    return render(request,'register.html',response)

def register_as_admin(request):
    response = {}
    return render(request,'register_admin.html',response)

def post_register_admin(request):
    ktp = str(request.POST['ktp'])
    if(len(ktp) != 16):
        return HttpResponseRedirect('/register/admin/')
    else:
        nama = request.POST['nama']
        email = request.POST['email']
        tanggal = request.POST['tanggal']
        telp = request.POST['telp']

        cursor = connection.cursor()
        cursor.execute('set search_path to toys_rent')
        cursor.execute("SELECT * from pengguna where email='"+email+"'")
        hasil = cursor.fetchone()
        if(hasil):
            if len(hasil) > 0:
                return HttpResponseRedirect('/register/admin/')

        cursor = connection.cursor()
        cursor.execute('set search_path to toys_rent')
        cursor.execute("SELECT * from admin where no_ktp='"+ktp+"'")
        hasil = cursor.fetchone()
        if(hasil):
            if len(hasil) > 0:
                return HttpResponseRedirect('/register/admin/')

        values = "'"+ktp+"','"+nama+"','"+email+"','"+tanggal+"','"+telp+"'"
        query = "INSERT INTO pengguna (no_ktp,nama_lengkap,email,tanggal_lahir,no_telp) values (" + values + ")"
        cursor.execute(query)
        values = "'"+ktp+"'"
        query = "INSERT INTO admin (no_ktp) values (" + values + ")"
        cursor.execute(query)
        return HttpResponseRedirect('/login/')

def register_as_anggota(request):
    response = {}
    return render(request,'register_anggota.html',response)

def post_register_anggota(request):
    ktp = str(request.POST['ktp'])
    if(len(ktp) != 16):
        return HttpResponseRedirect('/register/anggota/')
    else:
        nama = request.POST['nama']
        email = request.POST['email']
        tanggal = request.POST['tanggal']
        telp = request.POST['telp']
        alamat_nama = request.POST['alamat_nama']
        jalan = request.POST['alamat_jalan']
        nomor = request.POST['alamat_nomor']
        kota = request.POST['alamat_kota']
        kodepos = request.POST['alamat_kodepos']
        poin = '0'
        level = 'Bronze'

        cursor = connection.cursor()
        cursor.execute('set search_path to toys_rent')
        cursor.execute("SELECT * from pengguna where email='"+email+"'")
        hasil = cursor.fetchone()
        if(hasil):
            if len(hasil) > 0:
                return HttpResponseRedirect('/register/anggota/')

        cursor = connection.cursor()
        cursor.execute('set search_path to toys_rent')
        cursor.execute("SELECT * from anggota where no_ktp='"+ktp+"'")
        hasil = cursor.fetchone()
        if(hasil):
            if len(hasil) > 0:
                return HttpResponseRedirect('/register/anggota/')

        values = "'"+ktp+"','"+nama+"','"+email+"','"+tanggal+"','"+telp+"'"
        query = "INSERT INTO pengguna (no_ktp,nama_lengkap,email,tanggal_lahir,no_telp) values (" + values + ")"
        cursor.execute(query)
        values = "'"+ktp+"','"+poin+"','"+level+"'"
        query = "INSERT INTO anggota (no_ktp,poin,level) values (" + values + ")"
        cursor.execute(query)
        values = "'"+ktp+"','"+alamat_nama+"','"+jalan+"','"+nomor+"','"+kota+"','"+kodepos+"'"
        query = "INSERT INTO alamat (no_ktp_anggota,nama,jalan,nomor,kota,kodepos) values (" + values + ")"
        cursor.execute(query)
        return HttpResponseRedirect('/login/')